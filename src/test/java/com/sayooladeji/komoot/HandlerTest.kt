package com.sayooladeji.komoot

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doNothing
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.whenever
import com.sayooladeji.komoot.Handler.Companion.messageMap
import com.sayooladeji.komoot.dto.Message
import org.junit.Test
import org.mockito.Matchers.anySetOf
import org.mockito.Mockito.mock
import org.mockito.Mockito.spy
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import java.text.SimpleDateFormat
import java.util.concurrent.Executors
import kotlin.test.assertEquals

/**
 * @author oladeji
 */
class HandlerTest {

  @Test
  fun testFireNotifications_cacheCleared() {
    println("testFireNotifications_cacheCleared")

    val sut = spy(Handler()).apply {
      messageMap.clear()
      concurrent = false
      backgroundExecutor = Executors.newSingleThreadExecutor()
      transportWrapper = mock(TransportWrapper::class.java).apply { doNothing().whenever(this).send(any()) }
    }

    sut.handle(Message("Sayo Oladeji", "oladejioluwasayo@gmail.com", "I love Komoot",
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-01-04 20:00:00")))
    sut.handle(Message("Sayo Oladeji", "oladejioluwasayo@gmail.com", "I love Lagos",
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-01-04 20:01:00")))
    sut.handle(Message("John Bull", "johnbull@gmail.com", "I love John",
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-01-04 20:02:00")))

    var count = 0
    messageMap.forEach { count++ }
    assertEquals(2, count)

    sut.fireNotifications()

    var body = "Hi Sayo Oladeji, your friends are active!\n\n" +
        "Wednesday, 20:01\tI love Lagos\nWednesday, 20:00\tI love Komoot"
    verify(sut, times(1)).sendEmail(eq("Sayo Oladeji - Komoot Challenge"), eq(body), eq("oladejioluwasayo@gmail.com"),
        anySetOf(Message::class.java))

    body = "Hi John Bull, your friends are active!\n\nWednesday, 20:02\tI love John"
    verify(sut, times(1)).sendEmail(eq("Sayo Oladeji - Komoot Challenge"), eq(body), eq("johnbull@gmail.com"),
        anySetOf(Message::class.java))


    // Map cleaned
    reset(sut)
    sut.handle(Message("Sayo Oladeji", "oladejioluwasayo@gmail.com", "I love Kaduna",
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-01-04 20:07:00")))
    sut.handle(Message("Sayo Oladeji", "oladejioluwasayo@gmail.com", "I love Katsina",
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-01-04 20:08:00")))

    sut.fireNotifications()

    body = "Hi Sayo Oladeji, your friends are active!\n\n" +
        "Wednesday, 20:08\tI love Katsina\nWednesday, 20:07\tI love Kaduna"
    verify(sut, times(1)).sendEmail(eq("Sayo Oladeji - Komoot Challenge"), eq(body), eq("oladejioluwasayo@gmail.com"),
        anySetOf(Message::class.java))

    count = 0
    messageMap.values.forEach { it.forEach { count++ } }
    assertEquals(0, count)
  }

  @Test
  fun testFireNotifications_failedEmail() {
    println("testFireNotifications_failedEmail")

    val sut = spy(Handler()).apply {
      messageMap.clear()
      concurrent = false
      backgroundExecutor = Executors.newSingleThreadExecutor()
      transportWrapper = mock(TransportWrapper::class.java).apply {
        whenever(send(any())).thenThrow(RuntimeException("Unable to send email"))
      }
    }

    sut.handle(Message("Sayo Oladeji", "oladejioluwasayo@gmail.com", "I love Komoot",
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2017-01-04 20:00:00")))

    var count = 0
    messageMap.forEach { count++ }
    assertEquals(1, count)

    sut.fireNotifications()

    count = 0
    messageMap.forEach { count++ }
    assertEquals(1, count)
  }
}