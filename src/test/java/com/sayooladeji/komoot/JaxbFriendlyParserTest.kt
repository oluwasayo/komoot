package com.sayooladeji.komoot

import com.sayooladeji.komoot.dto.Message
import com.sayooladeji.komoot.dto.Sns
import org.junit.Test
import java.text.SimpleDateFormat
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

/**
 * @author oladeji
 */
class JaxbFriendlyParserTest {

  @Test
  fun testParseJson() {
    print("testParseJson")

    val message = JaxbFriendlyParser().parseJson(Message::class.java, "{\"name\": \"Sayo Oladeji\", " +
        "\"email\": \"oladejioluwasayo@gmail.com\", " +
        "\"message\": \"I love Komoot\", " +
        "\"timestamp\":\"2017-01-04T20:00:00\"}")
    assertEquals("oladejioluwasayo@gmail.com", message.email)
    assertEquals("2017-01-04 20:00:00", SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(message.timestamp))
  }

  @Test
  fun testParseJson_sampleFromAmazon() {
    print("testParseJson_sampleFromAmazon")

    val sampleInput = "{\r\n  \"Type\" : \"Notification\",\r\n  \"MessageId\" : \"d2e5b73b-77a5-51f7-\",\r\n  " +
        "\"TopicArn\" : \"arn:aws:sns:eu-west-1:963797398573:challenge-notifications\",\r\n  " +
        "\"Message\" : \"{\\\"timestamp\\\": \\\"2017-01-05T11:26:30\\\", " +
        "\\\"message\\\": \\\"Ilanov did a roadbike tour\\\", \\\"name\\\": \\\"Richard\\\", " +
        "\\\"email\\\": \\\"x58d+Richard@komoot.de\\\"}\",\r\n  \"Timestamp\" : \"2017-01-05T11:26:30.401Z\",\r\n  " +
        "\"SignatureVersion\" : \"1\",\r\n  \"Signature\" : \"EEgygm0itQUG/f/xPoiYByALpyLt53dQvF\",\r\n  " +
        "\"SigningCertURL\" : \"https://sns.eu-west-1.amazonaws.com\",\r\n  " +
        "\"UnsubscribeURL\" : \"https://sns.eu-west-1.amazonaws.com\"\r\n}"
    assertNotNull(JaxbFriendlyParser().parseJson(Sns::class.java, sampleInput))
  }
}

