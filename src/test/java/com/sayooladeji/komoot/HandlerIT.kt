package com.sayooladeji.komoot

import org.junit.Test
import kotlin.test.assertTrue

/**
 * @author oladeji
 */
class HandlerIT {

  @Test
  fun testSendEmail() {
    println("testSendEmail")

    Handler().apply {
      transportWrapper.send(toMimeMessage("Komoot Integration Test", "Testing mic.", "oladejioluwasayo@gmail.com"))
    }
    assertTrue(true)
  }
}