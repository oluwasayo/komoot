package com.sayooladeji.komoot

import java.util.HashSet
import javax.ws.rs.ApplicationPath
import javax.ws.rs.core.Application

/**
 * JAX-RS configuration class.
 *
 * @author oladeji
 */
@ApplicationPath("rpc")
class ApplicationConfig : Application() {

  override fun getClasses() = HashSet<Class<*>>().apply { addRestResourceClasses(this) }

  private fun addRestResourceClasses(resources: MutableSet<Class<*>>) {
    resources.add(com.fasterxml.jackson.jaxrs.base.JsonMappingExceptionMapper::class.java)
    resources.add(com.fasterxml.jackson.jaxrs.base.JsonParseExceptionMapper::class.java)
    resources.add(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider::class.java)
    resources.add(com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider::class.java)
    resources.add(com.fasterxml.jackson.jaxrs.json.JsonMappingExceptionMapper::class.java)
    resources.add(com.fasterxml.jackson.jaxrs.json.JsonParseExceptionMapper::class.java)
    resources.add(com.sayooladeji.komoot.Endpoint::class.java)
  }
}
