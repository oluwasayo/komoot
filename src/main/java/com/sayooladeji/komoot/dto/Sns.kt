package com.sayooladeji.komoot.dto

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

/**
 * POJO mapping for the JSON data received from SNS.
 *
 * @author oladeji
 */
@XmlRootElement
class Sns(
    @get:XmlElement(name = "Type") var type: String? = null,
    @get:XmlElement(name = "MessageId") var messageId: String? = null,
    @get:XmlElement(name = "TopicArn") var topicArn: String? = null,
    @get:XmlElement(name = "Message") var message: String? = null,
    @get:XmlElement(name = "Timestamp") var timestamp: String? = null,
    @get:XmlElement(name = "SignatureVersion") var signatureVersion: String? = null,
    @get:XmlElement(name = "Signature") var signature: String? = null,
    @get:XmlElement(name = "SigningCertURL") var signingCertUrl: String? = null,
    @get:XmlElement(name = "UnsubscribeURL") var unsubscribeUrl: String? = null,
    @get:XmlElement(name = "MessageAttributes") var messageAttributes: Map<String, String>? = null,
    @get:XmlElement(name = "Subject") var subject: String? = null
)
