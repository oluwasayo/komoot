package com.sayooladeji.komoot.dto

import com.sayooladeji.komoot.DateAdapter
import java.lang.Integer.signum
import java.util.Date
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

/**
 * POJO mapping for the actual event notification message pushed to SNS.
 * This datastructure has been tuned for making sorted collections preserve their order of insertion based on timestamp.
 *
 * @author oladeji
 */
@XmlRootElement
class Message(
    @get:XmlElement var name: String? = null,
    @get:XmlElement var email: String? = null,
    @get:XmlElement var message: String? = null,
    @get:XmlElement @get:XmlJavaTypeAdapter(DateAdapter::class) var timestamp: Date? = null
) : Comparable<Message> {

  override fun compareTo(other: Message): Int {
    return (signum(this.timestamp!!.compareTo(other.timestamp)) * 5)
        + signum((name + message).compareTo(other.name + other.message))
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other?.javaClass != javaClass) return false

    other as Message

    if (name != other.name) return false
    if (email != other.email) return false
    if (message != other.message) return false
    if (timestamp != other.timestamp) return false

    return true
  }

  override fun hashCode(): Int {
    var result = name?.hashCode() ?: 0
    result = 31 * result + (email?.hashCode() ?: 0)
    result = 31 * result + (message?.hashCode() ?: 0)
    result = 31 * result + (timestamp?.hashCode() ?: 0)
    return result
  }
}
