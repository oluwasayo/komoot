package com.sayooladeji.komoot

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.AnnotationIntrospector.pair
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector
import java.io.IOException
import javax.enterprise.context.ApplicationScoped

/**
 * @author oladeji
 */
@ApplicationScoped
class JaxbFriendlyParser() {

  private val jsonMapper = ObjectMapper().apply {
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
    setAnnotationIntrospector(pair(JacksonAnnotationIntrospector(), JaxbAnnotationIntrospector(typeFactory)))
  }

  @Throws(IOException::class, JsonParseException::class, JsonMappingException::class)
  fun <T> parseJson(t: Class<T>, json: String) = jsonMapper.readValue(json, t)
}
