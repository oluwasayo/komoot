package com.sayooladeji.komoot

import com.sayooladeji.komoot.dto.Message
import java.text.SimpleDateFormat
import java.util.Properties
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.ExecutorService
import javax.annotation.Resource
import javax.ejb.Schedule
import javax.ejb.Stateless
import javax.mail.Authenticator
import javax.mail.Message.RecipientType
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage


/**
 * Engine for processing messages.
 *
 * @author oladeji
 */
@Stateless
class Handler {

  @Resource(lookup = "java:comp/DefaultManagedScheduledExecutorService")
  internal lateinit var backgroundExecutor: ExecutorService

  internal var transportWrapper = TransportWrapper()
  internal var concurrent = true

  companion object {
    private val settings = Properties().apply { load(Handler::class.java.getResourceAsStream("/mail.properties")) }
    internal val messageMap = ConcurrentHashMap<String, ConcurrentSkipListSet<Message>>()
  }

  /**
   * Handles an incoming message.
   */
  internal fun handle(message: Message) {
    println("Message received: ${message.message}")
    messageMap.putIfAbsent(message.email, ConcurrentSkipListSet<Message>())
    messageMap.get(message.email)!!.add(message)
  }

  @Schedule(hour = "*", persistent = false)
  internal fun fireNotifications() {
    messageMap.forEach { email, events ->
      run {
        if (events.isEmpty()) return@run

        val builder = StringBuilder("Hi ${events.first().name}, your friends are active!\n\n")
        val messages = mutableSetOf<Message>()
        events.descendingSet().forEach {
          builder.append("${SimpleDateFormat("EEEEEE, HH:mm").format(it.timestamp)}\t${it.message}\n")
          messages.add(it)
        }

        val text = builder.deleteCharAt(builder.lastIndex).toString()
        sendEmail("Sayo Oladeji - Komoot Challenge", text, email, messages)
      }
    }
  }

  internal fun sendEmail(title: String, body: String, recipient: String, messages: Set<Message>) {
    val sendTask = {
      try {
        transportWrapper.send(toMimeMessage(body, recipient, title))
        messageMap.get(recipient)?.removeAll(messages)
        println("Email successfully sent to ${recipient}")
      } catch (ex: Exception) {
        println("Unable to send email! ${ex.message}")
      }
    }

    if (concurrent) backgroundExecutor.execute { sendTask() } else sendTask()
  }

  internal fun toMimeMessage(title: String, body: String, recipient: String) = MimeMessage(getSession()).apply {
    setFrom(InternetAddress("oladejioluwasayo@gmail.com", "Sayo Oladeji"))
    setRecipient(RecipientType.TO, InternetAddress(recipient, "Komoot"))
    setSubject(title)
    setText(body)
  }

  internal fun getSession() = Session.getInstance(settings, object : Authenticator() {
    override fun getPasswordAuthentication(): PasswordAuthentication {
      return PasswordAuthentication(settings.getProperty("sender"), settings.getProperty("password"))
    }
  })
}

open class TransportWrapper {

  open fun send(message: MimeMessage?) = if (message != null) Transport.send(message) else Unit
}