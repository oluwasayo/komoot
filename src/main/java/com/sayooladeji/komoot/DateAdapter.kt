package com.sayooladeji.komoot

import java.text.SimpleDateFormat
import java.util.Date
import javax.enterprise.context.ApplicationScoped
import javax.xml.bind.annotation.adapters.XmlAdapter

/**
 * JAXB adapter for transforming strings to dates and vice versa.
 *
 * @author oladeji
 */
@ApplicationScoped
class DateAdapter : XmlAdapter<String, Date>() {

  private val format = "yyyy-MM-dd'T'HH:mm:ss"

  @Throws(Exception::class)
  override fun marshal(input: Date) = SimpleDateFormat(format).format(input)

  @Throws(Exception::class)
  override fun unmarshal(input: String) = SimpleDateFormat(format).parse(input) // Because SDF is not threadsafe.
}
