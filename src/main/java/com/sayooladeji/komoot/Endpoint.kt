package com.sayooladeji.komoot

import com.sayooladeji.komoot.dto.Message
import com.sayooladeji.komoot.dto.Sns
import javax.ejb.EJB
import javax.inject.Inject
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.core.Response

/**
 * HTTP endpoint for receiving messages from SNS.
 *
 * @author oladeji
 */
@Path("endpoint")
class Endpoint {

  @EJB
  protected lateinit var handler: Handler

  @Inject
  protected lateinit var parser: JaxbFriendlyParser // Too bad because SNS does not send application/json

  @POST
  fun receive(payload: String) = Response.ok().build().apply {
    if (payload.contains("SubscriptionConfirmation")) println("SUBSCRIPTION CONFIRMATION: $payload")
    parser.parseJson(Sns::class.java, payload).apply {
      handler.handle(parser.parseJson(Message::class.java, this.message!!))
    }
  }
}
