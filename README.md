Komoot Challenge
--

The app was written in plain Java EE using Kotlin.
* A simple JAX-RS endpoint to receive SNS messages via HTTP.
* A stateless EJB using the timer service to send the hourly digests.
* Emails sent in a non-blocking manner using the default EE managed executor.
* Plain old JUnit + Mockito for testing and Jacoco for coverage.

To run the app on your local machine you only need to do the following:

```git clone https://bitbucket.org/oluwasayo/komoot.git```

```mvn clean install -Dsender=you@youremail.com -Dpassword=yourpassword -Dsmtp.host=your.smtp.provider -Dsmtp.port=port```

For example, for Yahoo! Mail, use `smtp.host=smtp.mail.yahoo.com` `smtp.port=587`

Running that command, tests the system then automatically downloads a fresh WildFly 10 server on which the app is 
deployed.
This command blocks until cancelled. To run the app over a long period of time
simply obtain the WAR file in `/target` and deploy it on any Java EE 7 server.

The endpoint will be published at `http://your_address:8080/komoot/rpc/endpoint`

Please note that the `sender` and `password` passed are built into the WAR file. 
This was only done for the sake of simplicity.
A real production app would have these kind of config in a secure datastore.
Also don’t forget that the command will be in your terminal’s history.
Be sure to protect your privacy.